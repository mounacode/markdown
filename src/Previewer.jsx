import './App.css';
import { marked } from 'marked';
marked.setOptions({
  breaks:true
});
const renderer= new marked.Renderer()
function Previewer({markdown}){
    return(
      <div id="preview" dangerouslySetInnerHTML={{
        __html: marked(markdown,{ renderer:renderer})
      }}  >
        
     
      </div>
    )
  }

  export default Previewer