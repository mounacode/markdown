
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useState } from 'react';
import Previewer from './Previewer';
function App() {
  const[text,setText] =useState('')
  return (
    <div className="text-center px-4">
      <h1 className="p-4">
        My MarkDown Previewer
      </h1>
      <textarea name="text" id="text" rows="10" value={text} className='textarea'
      onChange={(e)=>setText(e.target.value)}></textarea>
      <h3 className="mt-3">OutPut</h3>
      <Previewer markdown={text} className="hell"/>
    
    </div>
  );
}


export default App;
